var state= "Dashboard";
var usercity="";
var user={
    "Name":"guest",
    "login":"guest",
    "password":"guest",
    "image":"contents/icons/duke.png",
    "city":""
};

var pages={
    "home":true,
    "events":false,
    "transit":false,
    "hotels":false,
    "dining":false,
    "parking":false,
    "attractions":false,
    "topten":false,
    "museums":false,
    "themeparks":false,
    "calendar":false
}

var cityEvents=[
    {"name":"Lunch",
      "description":""
    },
    {"name":"FireWorks",
     "description":"Navy Pier"
    },
    {"name":"SkyDeck",
      "description":"Tour"
    },
    {"name":"SheddAquarium",
      "description":"Tour"
    },
    {"name":"Shopping",
      "description":""
    }
];

    
 

$( document ).ready(function() {
    console.log( "ready!" );
    $('#myModal').modal({backdrop: 'static', keyboard: false}) ;
    $('#myModal').modal('show');
    setUser(user);
    
    
});


setUser=function(user){
    
    elements= $(".username");
    for (i=0;i<elements.length;i++)
    {
            elements[i].innerHTML=user.Name;
    }
    
    image= $(".userimage");
    for (i=0;i<image.length;i++)
    {
            image[i].src=user.image;
    }
    
    if (user.Name=="guest")
    {
        $("#btn_signout")[0].innerHTML="SignIn";
    }
 
};




setCity=function()
{
    user["city"]=usercity;
    $("#pageHeader")[0].innerHTML=usercity.toLocaleUpperCase();
}

var updatePageView=function(id)
{   
    
    for(var key in pages)
    {
        if(id===key)
            {   pages[id]=true;
                
                print("#"+key);

                document.getElementById(id).setAttribute("style","display:block");
//                alert("show");
            }
        else
        {  console.log("false");
            pages[key]=false;
            print(key);
//              $("#"+key).hide();
            
             $("#"+key)[0].style="display:none";
        }
    }
    
}

print=function(data)
{
        console.log(data);
}

showElement=function(id,bool)
{
        print("showing elemenrt",id);
        if(bool)
            document.getElementById("ok").disabled="";
        else
            document.getElementById("ok").disabled="disabled";
}


updateOnlineStatus=function()
{
        console.log(navigator.onLine ? "online" : "offline");
}

window.addEventListener('online',  updateOnlineStatus);
window.addEventListener('offline', updateOnlineStatus);
